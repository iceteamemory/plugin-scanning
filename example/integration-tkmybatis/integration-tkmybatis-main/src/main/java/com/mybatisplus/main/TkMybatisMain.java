package com.mybatisplus.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 主启动程序
 *
 * @author starBlues
 * @version 1.0
 */
@SpringBootApplication()
public class TkMybatisMain {

    public static void main(String[] args) {
        SpringApplication.run(TkMybatisMain.class, args);
    }

}
