package com.gitee.starblues.factory;

/**
 * @author zhangzhuo
 * @version 2.4.3
 */
public class PropertyKey {

    public final static String INSTALL_AUTO_CONFIG_CLASS = "plugin.auto-config-class";

}
