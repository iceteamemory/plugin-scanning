cd springboot-plugin-framework
mvn clean deploy -Dmaven.test.skip=true

cd ../springboot-plugin-framework-extension/springboot-plugin-framework-extension-mybatis
mvn clean deploy -Dmaven.test.skip=true

cd ../springboot-plugin-framework-extension-resources
mvn clean deploy -Dmaven.test.skip=true